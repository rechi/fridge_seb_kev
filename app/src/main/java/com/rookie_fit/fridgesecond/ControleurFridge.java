package com.rookie_fit.fridgesecond;

import android.app.Activity;
import android.os.Handler;
import android.support.design.widget.BottomNavigationView;

import com.rookie_fit.fridgesecond.API.AppelAPI;
import com.rookie_fit.fridgesecond.Entity.Event;
import com.rookie_fit.fridgesecond.Entity.Fridge;
import com.rookie_fit.fridgesecond.Entity.Measures;
import com.rookie_fit.fridgesecond.Entity.User;

import java.util.List;

/**
 * Created by rechi on 07/02/2017.
 */

public class ControleurFridge {

    private Fridge moi;
    private List<Measures> measuresList;
    private static ControleurFridge control;
    private Activity context;
    private AppelAPI apel;
    private List<User> users;
    private List<Event> events;

    public static ControleurFridge getInstance(Activity a ,String serial){
        if (control == null){
            control = new ControleurFridge(a,serial);
        }
        return control;
    }

    public static ControleurFridge getInstance(){
        if (control == null){
            return null;
        }
        return control;
    }

    private ControleurFridge(Activity a,String serial){
        System.out.println("SERIAL --- " +serial);
        context = a;
        apel = new AppelAPI();
        API_CreateFridge(serial);
    }


    public Fridge getFridge(){
        return moi;
    }

    public void setFridge(Fridge frigo){
        moi = frigo;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users){
        this.users = users;
    }

    public List<Event> getEvent(){
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public synchronized List<Measures> getMeasuresList() {
        return measuresList;
    }

    public void setMeasuresList(List<Measures> measuresList) {
       // Collections.reverse(measuresList);
        this.measuresList = measuresList;

        if(context != null) {
           BottomNavigationView nvb = (BottomNavigationView) context.findViewById(R.id.NavigationBar);
           nvb.getMenu().getItem(0).setEnabled(true);
        }
    }

    public void API_PUT_Fridge() {
        apel.updateFridge(moi.getSerial());
    }

    public void API_GetMeasures(String serial, Handler handler,String from, String to) {

        System.out.println("API_GetMeasures from "+ from  + " to " + to);
        apel.getMeasures(serial,handler,from,to);
    }

    public void API_GetUsers(String serial,Handler handel) {
        apel.getUsers(serial,handel);
    }

    public void API_GetAlertes(String serial, Handler handler,String from ,String to) {
        apel.getEvents(serial,handler,from,to);
    }

    public void API_CreateFridge(String serial){
        apel.createFridge(serial);
    }

    public void API_GetMeasures(String serial){
        apel.getMeasures(serial);
    }

}
