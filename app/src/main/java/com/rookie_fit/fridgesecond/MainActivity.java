package com.rookie_fit.fridgesecond;

import android.Manifest;
import android.content.Intent;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import com.rookie_fit.fridgesecond.Fragments.Fragment_Dashboard;
import com.rookie_fit.fridgesecond.Fragments.Fragment_Database;
import com.rookie_fit.fridgesecond.Fragments.Fragment_Settings;
import com.rookie_fit.fridgesecond.Service.SensorsService;

public class MainActivity extends FragmentActivity {

    //Service
    private Intent sensorsServiceIntent;

    //Controleur
    private ControleurFridge controleur;

    //Fragments
    Fragment_Settings fragment_settings = new Fragment_Settings();
    Fragment_Dashboard fragment_dashboard = new Fragment_Dashboard();
    Fragment_Database fragment_database = new Fragment_Database();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView nvb = (BottomNavigationView) findViewById(R.id.NavigationBar);
        nvb.getMenu().getItem(0).setEnabled(false);
        nvb.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.settings:
                                changeFragment(fragment_settings);
                                break;
                            case R.id.dashboard:
                                changeFragment(fragment_dashboard);
                                break;
                            case R.id.database:
                                controleur.API_GetMeasures("dev");
                                changeFragment(fragment_database);
                                break;
                        }
                        return true;
                    }
                });


        changeFragment(fragment_dashboard);

        controleur = ControleurFridge.getInstance(this,getDeviceId(this.getBaseContext()));
        controleur.API_GetMeasures("dev");

       // startService();
    }

    @Override
    protected void onResume(){
        super.onResume();
        startReceiver();
        startService();
    }

    @Override
    protected void onPause(){
        super.onPause();
        stopReceiver();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService();
    }

    private void startService(){
        sensorsServiceIntent = new Intent(this, SensorsService.class);
        startService(sensorsServiceIntent);
    }

    private void stopService(){
        stopService(sensorsServiceIntent);
    }

    private void startReceiver(){
        SensorsReceiver receiver = SensorsReceiver.getInstance();
        receiver.attach(this);
        receiver.addCallbackSensorsReceiver(fragment_dashboard);
    }

    private void stopReceiver(){
        SensorsReceiver receiver = SensorsReceiver.getInstance();
        receiver.detached(this);
    }

    public static String getDeviceId(Context context) {
        //final String deviceId = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        String deviceId = null;
        if (deviceId != null) {
            return deviceId;
        } else {
            return android.os.Build.SERIAL;
        }
    }


    public void changeFragment(Fragment frag){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
