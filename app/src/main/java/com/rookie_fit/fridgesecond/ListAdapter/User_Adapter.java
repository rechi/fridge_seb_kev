package com.rookie_fit.fridgesecond.ListAdapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rookie_fit.fridgesecond.Entity.User;
import com.rookie_fit.fridgesecond.R;

import java.util.List;

/**
 * Created by kccrocher on 12/03/2017.
 */

public class User_Adapter extends BaseAdapter {

    private List<User> users;
    private Context context;

    public User_Adapter(Context context,List<User> users){
        this.users = users;
        this.context = context;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        User_Adapter.UserViewHolder viewHolder =  null;

        if(v==null) {
            v = View.inflate(context, R.layout.user_item, null);
            viewHolder = new User_Adapter.UserViewHolder();
            viewHolder.name = (TextView) v.findViewById(R.id.textViewnameuser);
            viewHolder.picture = (ImageView) v.findViewById(R.id.imageViewuserprofile);
            viewHolder.delete = (ImageView) v.findViewById(R.id.imageViewuserdelete);
            v.setTag(viewHolder);

        }else{
            viewHolder = (UserViewHolder) v.getTag();
        }

        viewHolder.name.setText(users.get(position).getSerial());// + "\n" +users.get(position).getGcm_key()
        viewHolder.picture.setImageResource(R.drawable.user);
        viewHolder.delete.setImageResource(R.drawable.paperbin);

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //remove
               // users.remove(position);
            }
        });


        return v;
    }

    class UserViewHolder{
        TextView name;
        ImageView picture;
        ImageView delete;
    }
}
