package com.rookie_fit.fridgesecond.ListAdapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rookie_fit.fridgesecond.ControleurFridge;
import com.rookie_fit.fridgesecond.Entity.Event;
import com.rookie_fit.fridgesecond.Entity.ValueEvent;
import com.rookie_fit.fridgesecond.R;

import java.util.List;

/**
 * Created by kccrocher on 19/03/2017.
 */

public class Alert_Adapter extends BaseAdapter {


    private Context context;
    private  List<Event> events;

    public Alert_Adapter(Context context , List<Event> events){
        this.context = context;
        this.events = events;
    }

    @Override
    public int getCount() {
        return events.size();
    }

    @Override
    public Object getItem(int position) {
        return events.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        AlertViewHolder viewHolder =  null;

        if(v==null) {
            v = View.inflate(context, R.layout.alerte_item, null);
            viewHolder = new AlertViewHolder();
            viewHolder.value = (TextView) v.findViewById(R.id.TextViewValueAlert);
            viewHolder.date = (TextView) v.findViewById(R.id.TextViewDateAlert);
            viewHolder.type = (ImageView) v.findViewById(R.id.imageViewtype);
            viewHolder.hautbas = (ImageView) v.findViewById(R.id.imageViewhautbas);
            viewHolder.flecheBas = (ImageView) v.findViewById(R.id.downarrow_alert_item);
            viewHolder.flecheHaut = (ImageView) v.findViewById(R.id.uparrow_alert_item);
            v.setTag(viewHolder);

        }else{
            viewHolder = (AlertViewHolder) v.getTag();
        }
        viewHolder.hautbas.setVisibility(View.VISIBLE);
        viewHolder.flecheBas.setVisibility(View.INVISIBLE);
        viewHolder.flecheHaut.setVisibility(View.INVISIBLE);

        Event event = (Event) getItem(position);
        ControleurFridge c = ControleurFridge.getInstance();

        if(event.getName().equals("temp_1")){

            viewHolder.type.setImageResource(R.drawable.thermometer);

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            ValueEvent value =  gson.fromJson(event.getValue(), ValueEvent.class);

            String hb = "";
            if(value.getType().equals("high")){
                viewHolder.hautbas.setImageResource(R.drawable.arrowupred);
                hb = "Trop haut ";
            }else{
                viewHolder.hautbas.setImageResource(R.drawable.arrowdownblue);
                hb = "Trop bas ";
            }

            viewHolder.flecheHaut.setVisibility(View.VISIBLE);
            viewHolder.value.setText(hb + value.getValue() + "°c");

        }else if(event.getName().equals("temp_2")){
            viewHolder.type.setImageResource(R.drawable.thermometer);
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            ValueEvent value =  gson.fromJson(event.getValue(), ValueEvent.class);

            String hb = "";
            if(value.getType().equals("high")){
                viewHolder.hautbas.setImageResource(R.drawable.arrowupred);
                hb = "Trop haut ";
            }else{
                viewHolder.hautbas.setImageResource(R.drawable.arrowdownblue);
                hb = "Trop bas ";
            }
            viewHolder.flecheBas.setVisibility(View.VISIBLE);
            viewHolder.value.setText(hb + value.getValue() + "°c");

        }else if(event.getName().equals("temp_3")){
            viewHolder.type.setImageResource(R.drawable.thermometer);
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            ValueEvent value =  gson.fromJson(event.getValue(), ValueEvent.class);

            String hb = "";
            if(value.getType().equals("high")){
                viewHolder.hautbas.setImageResource(R.drawable.arrowupred);
                hb = "Trop haut ";
            }else{
                viewHolder.hautbas.setImageResource(R.drawable.arrowdownblue);
                hb = "Trop bas ";
            }
            viewHolder.flecheBas.setVisibility(View.VISIBLE);
            viewHolder.flecheHaut.setVisibility(View.VISIBLE);
            viewHolder.value.setText(hb + value.getValue() + "°c");

        }else if(event.getName().equals("hygro")){
            viewHolder.type.setImageResource(R.drawable.water);
            String hb = "";
            if(Double.parseDouble(event.getValue()) > c.getFridge().getHygro_alert_max()){
                viewHolder.hautbas.setImageResource(R.drawable.arrowupred);
                hb = "Trop haut ";
            }else{
                viewHolder.hautbas.setImageResource(R.drawable.arrowdownblue);
                hb = "Trop bas ";
            }

            viewHolder.value.setText(hb + event.getValue() + "%");
        }else if(event.getName().equals("gas")){
            viewHolder.type.setImageResource(R.drawable.gas);
            String hb = "";
            if(Double.parseDouble(event.getValue()) > c.getFridge().getGas_alert_max()){
                viewHolder.hautbas.setImageResource(R.drawable.arrowupred);
                hb = "Trop haut ";
            }else{
                viewHolder.hautbas.setImageResource(R.drawable.arrowdownblue);
                hb = "Trop bas ";
            }
            viewHolder.value.setText(hb + event.getValue() + "%");
        }else if(event.getName().equals("door")){

            viewHolder.type.setImageResource(R.drawable.door);

            viewHolder.value.setText(event.getValue());
            viewHolder.hautbas.setVisibility(View.INVISIBLE);


        }

        viewHolder.date.setText(event.getCreatedAt().replace("T"," ").substring(0,event.getCreatedAt().lastIndexOf(".")));


        return v;
    }

    class AlertViewHolder{
        ImageView flecheHaut;
        ImageView flecheBas;
        ImageView type;
        ImageView hautbas;
        TextView value;
        TextView date;
    }
}
