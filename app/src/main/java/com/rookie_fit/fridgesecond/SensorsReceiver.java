package com.rookie_fit.fridgesecond;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;

import com.rookie_fit.fridgesecond.Entity.Measures;
import com.rookie_fit.fridgesecond.Service.SensorsService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 53js-Seb on 08/03/2017.
 */

public class SensorsReceiver extends BroadcastReceiver {

    private List<CallbackSensorsReceiver> callbacks = new ArrayList<>();
    private boolean isAttach;

    //Singleton
    private static SensorsReceiver singleton = new SensorsReceiver();
    private SensorsReceiver(){}
    public static SensorsReceiver getInstance(){
        return singleton;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean door = intent.getBooleanExtra(SensorsService.DataKeySensors.DOOR.getKey(), false);
        double temp1 = intent.getDoubleExtra(SensorsService.DataKeySensors.TEMP_0.getKey(), -1);
        double temp2 = intent.getDoubleExtra(SensorsService.DataKeySensors.TEMP_1.getKey(), -1.);
        double temp3 = intent.getDoubleExtra(SensorsService.DataKeySensors.TEMP_2.getKey(), -1.);
        double humidity = intent.getDoubleExtra(SensorsService.DataKeySensors.HUMIDITY.getKey(), -1);
        double gas = intent.getDoubleExtra(SensorsService.DataKeySensors.GAS.getKey(), -1);

        Measures measures = new Measures();
        measures.setOpen(door);
        measures.setTemp_1(temp1);
        measures.setTemp_2(temp2);
        measures.setTemp_3(temp3);
        measures.setHygro(humidity);
        measures.setGas(gas);

        updateMeasures(measures);
    }


    private void updateMeasures(final Measures measures){
        //Execute in the main thread
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                for(CallbackSensorsReceiver c : callbacks){
                    c.onReceiveMeasures(measures);
                }
            }
        });
    }

    public void addCallbackSensorsReceiver(CallbackSensorsReceiver c){
        callbacks.add(c);
    }

    public void removeCallbackSensorsReceiver(CallbackSensorsReceiver c){
        callbacks.remove(c);
    }

    public void attach(Context context){
        if(isAttach){
            return;
        }

        //Create filter
        IntentFilter filter = new IntentFilter();
        filter.addAction(context.getString(R.string.sensors_service_action));

        //Attach receiver
        context.registerReceiver(this, filter);
        isAttach = true;
    }

    public void detached(Context context) {
        if(!isAttach){
            return;
        }
        context.unregisterReceiver(this);
        isAttach = false;
        callbacks.clear();
    }

    public interface CallbackSensorsReceiver  {
        void onReceiveMeasures(Measures measures);
    }
}
