package com.rookie_fit.fridgesecond.Fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;
import com.rookie_fit.fridgesecond.ControleurFridge;
import com.rookie_fit.fridgesecond.Entity.Measures;
import com.rookie_fit.fridgesecond.MainActivity;
import com.rookie_fit.fridgesecond.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by kccrocher on 12/03/2017.
 */

public class Fragment_Database extends Fragment {

    private List<Measures> measures;
    public static Handler handle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.database, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Date d = c.getTime();
        c.add(Calendar.DAY_OF_MONTH, -7);
        Date d2 = c.getTime();



        majDate(d2,d);

        final Handler handler = new Handler() {
            public void handleMessage(Message msg) {

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {

                        MAJGRAPH();

                    }
                });
            }
        };

        handle = handler;

        addEventOnCalendar();

        addEventOnSpinner();

        addEventClickOnAlerte();

    }

    private void addEventClickOnAlerte() {

        ((Button) getActivity().findViewById(R.id.buttonalertes)).setEnabled(false);
        final Handler handler = new Handler() {
            public void handleMessage(Message msg) {

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        ((Button) getActivity().findViewById(R.id.buttonalertes)).setEnabled(true);
                        ((Button) getActivity().findViewById(R.id.buttonalertes)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ((MainActivity)getActivity()).changeFragment(new Fragment_Database_Alertes());
                            }
                        });
                    }
                });
            }
        };

        Calendar c = Calendar.getInstance();
        Date d = c.getTime();
        c.add(Calendar.DAY_OF_MONTH,-7);
        Date d2 = c.getTime();
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);

        ControleurFridge.getInstance().API_GetAlertes("dev",handler,df.format(d2),df.format(d));

    }

    private void addEventOnSpinner() {

        ((Spinner)getActivity().findViewById(R.id.spinner_data)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                MAJGRAPH();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void MAJGRAPH() {

        System.out.println("- -----    MAJ GRAPHE   ------------");

        measures = ControleurFridge.getInstance().getMeasuresList();

        Spinner spinner = ((Spinner)getActivity().findViewById(R.id.spinner_data));
        if(measures != null && measures.size() > 0) {
            if (spinner.getSelectedItem().toString().equals("Température")) {

                Create_Graph_Temperature();

            } else if (spinner.getSelectedItem().toString().equals("Gaz de décomposition")) {

                Create_Graph_Gaz();

            } else if (spinner.getSelectedItem().toString().equals("Ouverture porte")) {

                Create_Graph_Porte();

            } else if (spinner.getSelectedItem().toString().equals("Hygrométrie")) {

                Create_Graph_Hygrometrie();

            }

            String dateFin = (String) ((TextView) getActivity().findViewById(R.id.textViewdate2)).getText();
            String dateDebut = (String) ((TextView) getActivity().findViewById(R.id.textViewdate1)).getText();

            String monthstringFin = dateFin.split("/")[1];
            String daystringFin = dateFin.split("/")[0];

            String monthstringDebut = dateDebut.split("/")[1];
            String daystringDebut = dateDebut.split("/")[0];


            Calendar c = Calendar.getInstance();
            c.set(2017, Integer.valueOf(monthstringDebut) - 1, Integer.valueOf(daystringDebut), 0, 0);

            Calendar c2 = Calendar.getInstance();
            c2.set(2017, Integer.valueOf(monthstringFin) - 1, Integer.valueOf(daystringFin), 0, 0);

            System.out.println(c);
            System.out.println(c2);

            majDate(c.getTime(), c2.getTime());

            refresh_graph();
        }
    }

    private void refresh_graph(){

        GraphView graph = (GraphView) getActivity().findViewById(R.id.graph);
        graph.refreshDrawableState();
        graph.getGridLabelRenderer().invalidate(true,false);
        graph.postInvalidate();
        graph.invalidate();
    }

    private void Create_Graph_Temperature(){
        GraphView graph = createGraph("°C");
        graph.setTitle(" Température ");

        double maxTemp = 0;
        double minTemp = 0;
        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>();
        LineGraphSeries<DataPoint> series2 = new LineGraphSeries<DataPoint>();
        LineGraphSeries<DataPoint> series3 = new LineGraphSeries<DataPoint>();

        series.setTitle("Temperature 1");
        series.setColor(Color.BLUE);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(10);
        series.setBackgroundColor(Color.argb(50,0,0,255));
        series.setDrawBackground(true);
        series.setThickness(8);
        series.setDrawAsPath(true);
        series.setAnimated(true);
        series.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {

                Date d = new Date();
                d.setTime((long) dataPoint.getX());
                String date = new SimpleDateFormat("EE dd MMMM\nHH:mm").format(d);
                Toast.makeText(getActivity(), date + "  " + dataPoint.getY()+"°c ", Toast.LENGTH_SHORT).show();
            }
        });

        series2.setTitle("Temperature 2");
        series2.setColor(Color.GREEN);
        series2.setDrawDataPoints(true);
        series2.setDataPointsRadius(10);
        series2.setBackgroundColor(Color.argb(50,0,255,0));
        series2.setDrawBackground(true);
        series2.setThickness(8);
        series2.setDrawAsPath(true);
        series2.setAnimated(true);
        series2.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {


                Date d = new Date();
                d.setTime((long) dataPoint.getX());
                String date = new SimpleDateFormat("EE dd MMMM\nHH:mm").format(d);
                Toast.makeText(getActivity(), date + "  " + dataPoint.getY()+"°c ", Toast.LENGTH_SHORT).show();
            }
        });

        series3.setTitle("Temperature 3");
        series3.setColor(Color.RED);
        series3.setDrawDataPoints(true);
        series3.setDataPointsRadius(10);
        series3.setBackgroundColor(Color.argb(50,255,0,0));
        series3.setDrawBackground(true);
        series3.setThickness(8);
        series3.setDrawAsPath(true);
        series3.setAnimated(true);
        series3.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {

                Date d = new Date();
                d.setTime((long) dataPoint.getX());
                String date = new SimpleDateFormat("EE dd MMMM\nHH:mm").format(d);
                Toast.makeText(getActivity(), date + "  " + dataPoint.getY()+"°c ", Toast.LENGTH_SHORT).show();
            }
        });

        for(Measures measure : measures){
            String string = measure.getCreatedAt();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date date = null;
            try {
                date = format.parse(string);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            series.appendData(new DataPoint(date, measure.getTemp_1()),true,measures.size());
            series2.appendData(new DataPoint(date, measure.getTemp_2()),true,measures.size());
            series3.appendData(new DataPoint(date, measure.getTemp_3()),true,measures.size());

            if( minTemp > measure.getTemp_1()){
                minTemp = measure.getTemp_1();
            }
            if(minTemp > measure.getTemp_2()){
                minTemp = measure.getTemp_2();
            }
            if(minTemp > measure.getTemp_3()){
                minTemp = measure.getTemp_3();
            }

            if( maxTemp < measure.getTemp_1()){
                maxTemp = measure.getTemp_1();
            }
            if(maxTemp < measure.getTemp_2()){
                maxTemp = measure.getTemp_2();
            }
            if(maxTemp < measure.getTemp_3()){
                maxTemp = measure.getTemp_3();
            }
        }

        Measures measureFirst = measures.get(0);
        Measures measureLast = measures.get(measures.size()-1);

        String string = measureFirst.getCreatedAt();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date dateFirst = null;
        try {
            dateFirst = format.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        string = measureLast.getCreatedAt();
        Date dateLast = null;
        try {
            dateLast = format.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

      //  majDate(dateFirst,dateLast);

        graph.getViewport().setMinY(minTemp);
        graph.getViewport().setMaxY(maxTemp);

        graph.addSeries(series);
        graph.addSeries(series2);
        graph.addSeries(series3);
    }

    private void Create_Graph_Hygrometrie(){
        GraphView graph =  createGraph("% ");
        graph.setTitle(" Hygrométrie ");

        double maxTemp = 0;
        double minTemp = 0;

        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>();

        series.setTitle("Hygrométrie");
        series.setColor(Color.BLUE);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(10);
        series.setBackgroundColor(Color.argb(50,0,0,255));
        series.setDrawBackground(true);
        series.setThickness(8);
        series.setDrawAsPath(true);
        series.setAnimated(true);
        series.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {


                Date d = new Date();
                d.setTime((long) dataPoint.getX());
                String date = new SimpleDateFormat("EE dd MMMM\nHH:mm").format(d);
                Toast.makeText(getActivity(), date + "  " + dataPoint.getY()+"% ", Toast.LENGTH_SHORT).show();
            }
        });
        for(Measures measure : measures){
            String string = measure.getCreatedAt();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date date = null;
            try {
                date = format.parse(string);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            series.appendData(new DataPoint(date, measure.getHygro()),true,measures.size());

            if( minTemp > measure.getHygro()){
                minTemp = measure.getHygro();
            }

            if( maxTemp < measure.getHygro()){
                maxTemp = measure.getHygro();
            }
        }

        Measures measureFirst = measures.get(0);
        Measures measureLast = measures.get(measures.size()-1);

        String string = measureFirst.getCreatedAt();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date dateFirst = null;
        try {
            dateFirst = format.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        string = measureLast.getCreatedAt();
        Date dateLast = null;
        try {
            dateLast = format.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

      //  majDate(dateFirst,dateLast);

        graph.getViewport().setMinY(minTemp);
        graph.getViewport().setMaxY(maxTemp);

        graph.addSeries(series);

    }

    private void Create_Graph_Porte(){
        GraphView graph =  createGraph("s ");
        graph.setTitle(" Ouverture de porte ");
    }

    private void Create_Graph_Gaz(){
        GraphView graph =  createGraph("% ");
        graph.setTitle(" Gaz de décomposition ");

        double maxTemp = 0;
        double minTemp = 0;
        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>();

        series.setTitle("Gaz");
        series.setColor(Color.BLUE);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(10);
        series.setBackgroundColor(Color.argb(50,0,0,255));
        series.setDrawBackground(true);
        series.setThickness(8);
        series.setDrawAsPath(true);
        series.setAnimated(true);
        series.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {


                Date d = new Date();
                d.setTime((long) dataPoint.getX());
                String date = new SimpleDateFormat("EE dd MMMM\nHH:mm").format(d);
                Toast.makeText(getActivity(), date + "  " + dataPoint.getY()+"% ", Toast.LENGTH_SHORT).show();
            }
        });
        for(Measures measure : measures){
            String string = measure.getCreatedAt();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date date = null;
            try {
                date = format.parse(string);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println(date);
            System.out.println(string);
            series.appendData(new DataPoint(date, measure.getGas()),true,measures.size());

            if( minTemp > measure.getGas()){
                minTemp = measure.getGas();
            }

            if( maxTemp < measure.getGas()){
                maxTemp = measure.getGas();
            }
        }

        Measures measureFirst = measures.get(0);
        Measures measureLast = measures.get(measures.size()-1);

        String string = measureFirst.getCreatedAt();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date dateFirst = null;
        try {
            dateFirst = format.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        string = measureLast.getCreatedAt();
        Date dateLast = null;
        try {
            dateLast = format.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

       // majDate(dateFirst,dateLast);

        graph.getViewport().setMinY(minTemp);
        graph.getViewport().setMaxY(maxTemp);

        graph.addSeries(series);
    }

    private void majDate(Date dateFirst, Date dateLast){

        GraphView graph = (GraphView) getActivity().findViewById(R.id.graph);

        graph.getViewport().setMinX(dateFirst.getTime());
        graph.getViewport().setMaxX(dateLast.getTime());
        DateFormat format2 = new SimpleDateFormat("dd/MM");
        ((TextView)getActivity().findViewById(R.id.textViewdate1)).setText(format2.format(dateFirst));
        ((TextView)getActivity().findViewById(R.id.textViewdate2)).setText(format2.format(dateLast));

    }

    private void addEventOnCalendar() {

        getActivity().findViewById(R.id.datePicker1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment_DatePicker newFragment = new Fragment_DatePicker();
                Bundle args = new Bundle();
                args.putInt("text", R.id.textViewdate1);
                args.putString("min_max","min");
                args.putString("date", (String) ((TextView)getActivity().findViewById(R.id.textViewdate1)).getText());
                args.putString("date_debut", (String) ((TextView)getActivity().findViewById(R.id.textViewdate1)).getText());
                args.putString("date_fin",(String) ((TextView)getActivity().findViewById(R.id.textViewdate2)).getText());
                newFragment.setArguments(args);
                newFragment.show(getFragmentManager(), "datePicker");

            }
        });

        getActivity().findViewById(R.id.datePicker2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment_DatePicker newFragment = new Fragment_DatePicker();
                Bundle args = new Bundle();
                args.putInt("text", R.id.textViewdate2);
                args.putString("min_max","max");
                args.putString("date", (String) ((TextView)getActivity().findViewById(R.id.textViewdate2)).getText());
                args.putString("date_debut", (String) ((TextView)getActivity().findViewById(R.id.textViewdate1)).getText());
                args.putString("date_fin",(String) ((TextView)getActivity().findViewById(R.id.textViewdate2)).getText());
                newFragment.setArguments(args);
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });
    }

    public GraphView createGraph(final String typedataY){

        GraphView graph = (GraphView) getActivity().findViewById(R.id.graph);

        graph.getGridLabelRenderer().setGridColor(Color.BLACK);
        graph.getGridLabelRenderer().setLabelsSpace(10);
        graph.getGridLabelRenderer().setPadding(10);
        graph.getGridLabelRenderer().setHorizontalLabelsAngle(45);
        graph.getGridLabelRenderer().setTextSize(35);
        graph.getGridLabelRenderer().setNumHorizontalLabels(5);

        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    Date d = new Date();
                    d.setTime((long)value);
                    return   new SimpleDateFormat("EE dd MMMM\nHH:mm:ss").format(d);
                    // return super.formatLabel(value, isValueX);
                } else {
                    // show currency for y values
                    return super.formatLabel(value, isValueX) + typedataY;
                }
            }
        });

        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setScalable(true);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setBackgroundColor(Color.WHITE);

        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);

        graph.onDataChanged(false,false);

        graph.removeAllSeries();
        graph.getSeries().clear();

        return graph;
    }
}
