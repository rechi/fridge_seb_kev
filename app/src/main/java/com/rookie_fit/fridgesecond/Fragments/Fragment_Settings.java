package com.rookie_fit.fridgesecond.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.rookie_fit.fridgesecond.ControleurFridge;
import com.rookie_fit.fridgesecond.Entity.Fridge;
import com.rookie_fit.fridgesecond.MainActivity;
import com.rookie_fit.fridgesecond.R;

/**
 * Created by kccrocher on 12/03/2017.
 */

public class Fragment_Settings extends Fragment {

    ControleurFridge controleur;

    private int valueDepartTTH = 6;
    private int valueDepartTTB = -10;
    private int valueDepartHTH = 25;
    private int valueDepartGTH = 1;
    private int valueDepartPO = 0;
    private int valueDepartRA = 5;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.settings, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        controleur = ControleurFridge.getInstance();

        addListeneronSlider();
        addListeneronLayout();
        addListenerSwitcher();
        addEventClickButtonValider();

        ((TextView)getActivity().findViewById(R.id.textviewassoappareil)).setText(controleur.getFridge().getAssociation_key());

        ((SeekBar)getActivity().findViewById(R.id.seekBartth)).setProgress(Math.abs(valueDepartTTH - controleur.getFridge().getTemp_alert_max()));
        ((SeekBar)getActivity().findViewById(R.id.seekBarttb)).setProgress(Math.abs(valueDepartTTB - controleur.getFridge().getTemp_alert_min()));
        ((SeekBar)getActivity().findViewById(R.id.seekBarhth)).setProgress(Math.abs(valueDepartHTH - controleur.getFridge().getHygro_alert_max()));
        ((SeekBar)getActivity().findViewById(R.id.seekBargth)).setProgress(Math.abs(valueDepartGTH - controleur.getFridge().getGas_alert_max()));
        ((SeekBar)getActivity().findViewById(R.id.seekBarpo)).setProgress(Math.abs(valueDepartPO - controleur.getFridge().getOpen_alert_time()));
        ((SeekBar)getActivity().findViewById(R.id.seekBarra)).setProgress(Math.abs(valueDepartRA - (controleur.getFridge().getRemind_me_after()/60)));

        ((Switch)getActivity().findViewById(R.id.switchtth)).setChecked(controleur.getFridge().isTemp_alert_max_on());
        ((Switch)getActivity().findViewById(R.id.switchttb)).setChecked(controleur.getFridge().isTemp_alert_min_on());
        ((Switch)getActivity().findViewById(R.id.switchhth)).setChecked(controleur.getFridge().isHygro_alert_on());
        ((Switch)getActivity().findViewById(R.id.switchgth)).setChecked(controleur.getFridge().isGas_alert_on());
        ((Switch)getActivity().findViewById(R.id.switchpo)).setChecked(controleur.getFridge().isOpen_alert_on());

    }

    private void addListenerSwitcher() {
        ((Switch)getActivity().findViewById(R.id.switchtth)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                controleur.getFridge().setTemp_alert_max_on(isChecked);
            }
        });

        ((Switch)getActivity().findViewById(R.id.switchttb)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                controleur.getFridge().setTemp_alert_min_on(isChecked);
            }
        });
        ((Switch)getActivity().findViewById(R.id.switchhth)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                controleur.getFridge().setHygro_alert_on(isChecked);
            }
        });
        ((Switch)getActivity().findViewById(R.id.switchgth)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                controleur.getFridge().setGas_alert_on(isChecked);
            }
        });
        ((Switch)getActivity().findViewById(R.id.switchpo)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                controleur.getFridge().setOpen_alert_on(isChecked);
            }
        });
    }

    private void addEventClickButtonValider() {
        ((Button)getActivity().findViewById(R.id.buttonvalidermodif)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controleur.API_PUT_Fridge();
            }
        });
    }

    private void addListeneronSlider() {

        ((SeekBar)getActivity().findViewById(R.id.seekBartth)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Fridge f = controleur.getFridge();
                f.setTemp_alert_max(i + valueDepartTTH);
                System.out.println(valueDepartTTH + "  " + i);
                ((TextView)getActivity().findViewById(R.id.textViewtth2)).setText((i + valueDepartTTH) + "°c");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ((SeekBar)getActivity().findViewById(R.id.seekBarttb)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Fridge f = controleur.getFridge();
                f.setTemp_alert_min(i + valueDepartTTB);
                System.out.println("LOL " + valueDepartTTB + "  " + i);
                ((TextView)getActivity().findViewById(R.id.textViewttb2)).setText((i + valueDepartTTB) + "°c");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ((SeekBar)getActivity().findViewById(R.id.seekBarhth)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Fridge f = controleur.getFridge();
                f.setHygro_alert_max(i + valueDepartHTH);
                System.out.println("LOL " + valueDepartHTH + "  " + i);
                ((TextView)getActivity().findViewById(R.id.textViewhth2)).setText((i + valueDepartHTH) + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ((SeekBar)getActivity().findViewById(R.id.seekBargth)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Fridge f = controleur.getFridge();
                f.setGas_alert_max(i + valueDepartGTH);
                System.out.println("GAZ " + valueDepartGTH + "  " + i + "  -- " +f.getGas_alert_max() );
                ((TextView)getActivity().findViewById(R.id.textViewgth2)).setText((i + valueDepartGTH) + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ((SeekBar)getActivity().findViewById(R.id.seekBarra)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Fridge f = controleur.getFridge();
                f.setRemind_me_after((i + valueDepartRA) * 60);
                System.out.println("LOL " + valueDepartRA + "  " + i);
                ((TextView)getActivity().findViewById(R.id.textViewra2)).setText((i + valueDepartRA) + "min");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ((SeekBar)getActivity().findViewById(R.id.seekBarpo)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Fridge f = controleur.getFridge();
                f.setOpen_alert_time(i + valueDepartPO);
                System.out.println("LOL " + valueDepartPO + "  " + i);
                ((TextView)getActivity().findViewById(R.id.textViewpo2)).setText((i + valueDepartPO) + "°c");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    private void addListeneronLayout() {

        LinearLayout layout = (LinearLayout) getActivity().findViewById(R.id.assoappareil);
        layout.setEnabled(false);
        final Handler handler = new Handler() {
            public void handleMessage(Message msg) {

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout layout = (LinearLayout) getActivity().findViewById(R.id.assoappareil);
                        layout.setEnabled(true);
                        layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ((MainActivity)getActivity()).changeFragment(new Fragment_Settings_Associate());
                            }
                        });
                    }
                });
            }
        };

        ControleurFridge.getInstance().API_GetUsers("dev",handler);

    }
}
