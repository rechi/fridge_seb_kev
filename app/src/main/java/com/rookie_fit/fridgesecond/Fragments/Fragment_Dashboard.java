package com.rookie_fit.fridgesecond.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rookie_fit.fridgesecond.Entity.Measures;
import com.rookie_fit.fridgesecond.MainActivity;
import com.rookie_fit.fridgesecond.R;
import com.rookie_fit.fridgesecond.SensorsReceiver;

import java.text.DecimalFormat;

/**
 * Created by kccrocher on 12/03/2017.
 */

public class Fragment_Dashboard extends Fragment implements SensorsReceiver.CallbackSensorsReceiver{

    //Components
    private DecimalFormat textFormat = new DecimalFormat("#.##");
    private TextView highTemp;
    private TextView middleTemp;
    private TextView lowTemp;
    private TextView humidity;
    private TextView gas;
    private TextView door;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        LinearLayout layout = (LinearLayout)inflater.inflate(R.layout.dashboard, container, false);

        highTemp = (TextView)layout.findViewById(R.id.temperaturehaute);
        middleTemp = (TextView)layout.findViewById(R.id.temperaturemilieu);
        lowTemp = (TextView)layout.findViewById(R.id.temperaturebasse);
        humidity = (TextView)layout.findViewById(R.id.hygrometrie);
        gas = (TextView)layout.findViewById(R.id.gazdedecomposition);
        door = (TextView)layout.findViewById(R.id.porteouverte);

        return layout;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        addEventOnDownloadApp();
    }

    private void addEventOnDownloadApp() {

        ((TextView)getActivity().findViewById(R.id.downloadapp)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("CliecDownload ");
                ((MainActivity)getActivity()).changeFragment(new Fragment_Settings());
            }
        });

    }

    @Override
    public void onReceiveMeasures(Measures measures) {
        highTemp.setText(textFormat.format(measures.getTemp_1()));
        middleTemp.setText(textFormat.format(measures.getTemp_2()));
        lowTemp.setText(textFormat.format(measures.getTemp_3()));
        humidity.setText(textFormat.format(measures.getHygro()));
        gas.setText(textFormat.format(measures.getGas()));
        door.setText(String.valueOf(measures.isOpen()));
    }
}



