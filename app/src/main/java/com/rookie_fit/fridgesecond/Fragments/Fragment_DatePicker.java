package com.rookie_fit.fridgesecond.Fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.rookie_fit.fridgesecond.ControleurFridge;
import com.rookie_fit.fridgesecond.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by rechi on 13/03/2017.
 */

public class Fragment_DatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    private int month;
    private int day;
    private TextView text;
    private String min_max;
    private String date_debut;
    private String date_fin;
    private Handler handler;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);

        month = Integer.parseInt(getArguments().getString("date").split("/")[1].replace(" ","")) -1;
        day = Integer.parseInt(getArguments().getString("date").split("/")[0].replace(" ",""));

        date_debut = getArguments().getString("date_debut").replace(" ","");
        date_fin = getArguments().getString("date_fin").replace(" ","");

        text = (TextView) getActivity().findViewById(getArguments().getInt("text"));
        min_max = getArguments().getString("min_max");



        handler = Fragment_Database.handle;

        System.out.println("HANDLER --- > " + handler );
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {

        GraphView graph = (GraphView) getActivity().findViewById(R.id.graph);
        this.month = month;
        this.day = day;
        String monthstring = String.valueOf(month+1);//++1
        String daystring = String.valueOf(day);

        Calendar c = Calendar.getInstance();
        c.set(year, month, day, 0, 0);

        if(month < 10){
            monthstring = "0"+monthstring;
        }
        if(day < 10){
            daystring = "0"+daystring;
        }

        //-------------------------------------------------------------
        boolean valideDate = true;
        if(min_max.equals("min")) {
            if (Integer.parseInt(date_fin.split("/")[1]) < (month + 1)) {
                // probléme
                valideDate = false;
                Toast.makeText(getActivity(),"La date de debut doit être inféreur a celle de fin", Toast.LENGTH_SHORT).show();
            }else if(Integer.parseInt(date_fin.split("/")[1]) == (month + 1)){
                if (Integer.parseInt(date_fin.split("/")[0]) < (day)) {
                    // probléme
                    valideDate = false;
                    Toast.makeText(getActivity(),"La date de debut doit être inféreur a celle de fin", Toast.LENGTH_SHORT).show();
                }
            }
        }else if(min_max.equals("max")){
            if (Integer.parseInt(date_debut.split("/")[1]) > (month + 1)) {
                // probléme
                valideDate = false;
                Toast.makeText(getActivity(),"La date de fin doit être superieur a celle de début", Toast.LENGTH_SHORT).show();
            }else if(Integer.parseInt(date_debut.split("/")[1]) == (month + 1)){
                if (Integer.parseInt(date_debut.split("/")[0]) > (day)) {
                    // probléme
                    valideDate = false;
                    Toast.makeText(getActivity(),"La date de fin doit être superieur a celle de début", Toast.LENGTH_SHORT).show();
                }
            }
        }
        //-------------------------------------------------------------
        if( valideDate) {
            if (min_max.equals("min")) {
                System.out.println("--->  CHANGE " + c.getTime().getTime());
                graph.getViewport().setMinX(c.getTime().getTime());
                DateFormat format2 = new SimpleDateFormat("dd/MM");
                ((TextView)getActivity().findViewById(R.id.textViewdate1)).setText(format2.format(c.getTime()));


            } else if (min_max.equals("max")) {
                System.out.println("--->  CHANGE " + c.getTime().getTime());
                graph.getViewport().setMaxX(c.getTime().getTime());
                DateFormat format2 = new SimpleDateFormat("dd/MM");
                ((TextView)getActivity().findViewById(R.id.textViewdate2)).setText(format2.format(c.getTime()));
            }

            graph.refreshDrawableState();
            graph.getGridLabelRenderer().invalidate(true, false);
            graph.postInvalidate();
            graph.invalidate();

            text.setText(daystring + "/" + monthstring);

            TimeZone tz = TimeZone.getTimeZone("UTC");
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
            df.setTimeZone(tz);

            Calendar c2 = Calendar.getInstance();
            c2.set(year, Integer.parseInt(date_debut.split("/")[1]) -1, Integer.parseInt(date_debut.split("/")[0]), 0, 0);


            if (min_max.equals("min")) {
                System.out.println("Debut" + df.format(c.getTime()));
                System.out.println("FIN" + df.format(c2.getTime()));
                ControleurFridge.getInstance().API_GetMeasures(ControleurFridge.getInstance().getFridge().getSerial(),handler,df.format(c.getTime()),df.format(c2.getTime()));
            }else{
                c.set(year, month, day+1, 0, 0);
                System.out.println("Debut" + df.format(c2.getTime()));
                System.out.println("FIN" + df.format(c.getTime()));

                ControleurFridge.getInstance().API_GetMeasures(ControleurFridge.getInstance().getFridge().getSerial(),handler,df.format(c2.getTime()),df.format(c.getTime()));
            }

        }
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }
}
