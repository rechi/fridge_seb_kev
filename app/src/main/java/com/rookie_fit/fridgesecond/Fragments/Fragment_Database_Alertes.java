package com.rookie_fit.fridgesecond.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.rookie_fit.fridgesecond.ControleurFridge;
import com.rookie_fit.fridgesecond.ListAdapter.Alert_Adapter;
import com.rookie_fit.fridgesecond.R;

/**
 * Created by kccrocher on 19/03/2017.
 */

public class Fragment_Database_Alertes extends Fragment {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.database_alertes, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        addEventClickRetour();

        ListView myList = (ListView)  getActivity().findViewById(R.id.listAlertes);
        
        BaseAdapter adapter = new Alert_Adapter(getActivity(), ControleurFridge.getInstance().getEvent());
        myList.setAdapter(adapter);

    }

    private void addEventClickRetour() {

        ((Button)getActivity().findViewById(R.id.buttonalertesretour)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

    }
}
