package com.rookie_fit.fridgesecond.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.rookie_fit.fridgesecond.ControleurFridge;
import com.rookie_fit.fridgesecond.ListAdapter.User_Adapter;
import com.rookie_fit.fridgesecond.R;

/**
 * Created by kccrocher on 12/03/2017.
 */

public class Fragment_Settings_Associate extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.settings_associate, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getActivity().findViewById(R.id.assoappareilretour).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ((MainActivity)getActivity()).changeFragment(new Fragment_Settings());
                getFragmentManager().popBackStack();
            }
        });

        ((TextView)getActivity().findViewById(R.id.setting_asso_code_association)).setText(ControleurFridge.getInstance().getFridge().getAssociation_key());

        ListView myList = (ListView)  getActivity().findViewById(R.id.listUsers);
        BaseAdapter adapter = new User_Adapter(getActivity(), ControleurFridge.getInstance().getUsers());
        myList.setAdapter(adapter);
    }
}
