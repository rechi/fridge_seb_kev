package com.rookie_fit.fridgesecond.Service;

import android.util.Log;

import com.rookie_fit.fridgesecond.CmdExecutor;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by 53js-Seb on 19/03/2017.
 */

public class DoorReader {

    private static final String TAG = "DoorReader";

    private static native int readDoorState(int serialNumber);

    private int port;

    private Process p = null;

    public void start(int port){
        this.port = port;
        try {
            p = Runtime.getRuntime().exec(new String[]{"su", "-c", "system/bin/sh"});
            DataOutputStream stdin = new DataOutputStream(p.getOutputStream());
            stdin.writeBytes("echo " + port + " > /sys/class/gpio/export\n"); // \n executes the command
            stdin.writeBytes("echo out > /sys/class/gpio/gpio" + port + "/direction\n");
            stdin.flush();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    public boolean readDoor(){
        byte[] buffer = new byte[1024];
        int read;
        StringBuilder out = new StringBuilder();
        try{
            //Update value
            DataOutputStream stdin = new DataOutputStream(p.getOutputStream());
            stdin.writeBytes("cat /sys/class/gpio/gpio" + port + "/value\n");
            stdin.flush();

            //Read the value
            InputStream stdout = p.getInputStream();
            while(stdout.available() > 0){
                read = stdout.read(buffer);
                out.append(new String(buffer, 0, read));
            }

            if(out.length() < 1){
               return false;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.i(TAG, "DoorReader value : " + out.toString().charAt(0));
        return !(out.toString().charAt(0) == '1');
    }


    public void stop(){
        CmdExecutor.execute("echo " + port + " > /sys/class/gpio/unexport");
    }
}
