package com.rookie_fit.fridgesecond.Service;

import android.app.NotificationManager;
import android.os.Bundle;
import android.util.Log;

import com.rookie_fit.fridgesecond.API.AppelAPI;
import com.rookie_fit.fridgesecond.ControleurFridge;
import com.rookie_fit.fridgesecond.Entity.Event;
import com.rookie_fit.fridgesecond.Entity.Fridge;
import com.rookie_fit.fridgesecond.Entity.Measures;

import java.util.Date;
import java.util.TimerTask;

/**
 * Created by 53js-Seb on 21/03/2017.
 */

public class NotificationService extends TimerTask {

    private static final String TAG = "NotificationService";

    private AppelAPI appelAPI = new AppelAPI();
    private SensorsService sensorsService;

    private boolean lastDoorIsOpen;
    private boolean isNotifTemp1Up = true;
    private boolean isNotifTemp2Up = true;
    private boolean isNotifTemp3Up = true;
    private boolean isNotifHygroUp = true;
    private boolean isNotifGasUp = true;

    private Date openDate;

    public NotificationService(SensorsService sensorsService) {
        this.sensorsService = sensorsService;
    }

    @Override
    public void run() {
        Fridge fridge = ControleurFridge.getInstance().getFridge();
        if(fridge != null){
            ControleurFridge.getInstance().API_CreateFridge(fridge.getSerial());
        }

        isNotifTemp1Up = true;
        isNotifTemp2Up = true;
        isNotifTemp3Up = true;
        isNotifGasUp = true;
        isNotifHygroUp = true;

        Log.i(TAG, "Update fridge");
    }


    public void updateDataSensor(Bundle data){
        Log.i(TAG, "Update data sensor");
        Fridge fridge = ControleurFridge.getInstance().getFridge();

        if(fridge == null){
            Log.i(TAG, "Fridge is null...");
            return;
        }

        Measures measures = getMeasuresFromBundle(data);
        verifyTemp(fridge, measures);
        verifyHumidity(fridge, measures);
        verifyGas(fridge, measures);
        verifyDoor(fridge, measures);
    }

    private Measures getMeasuresFromBundle(Bundle bundle){
        boolean door = bundle.getBoolean(SensorsService.DataKeySensors.DOOR.getKey(), false);
        double temp1 = bundle.getDouble(SensorsService.DataKeySensors.TEMP_0.getKey(), -1);
        double temp2 = bundle.getDouble(SensorsService.DataKeySensors.TEMP_1.getKey(), -1.);
        double temp3 = bundle.getDouble(SensorsService.DataKeySensors.TEMP_2.getKey(), -1.);
        double humidity = bundle.getDouble(SensorsService.DataKeySensors.HUMIDITY.getKey(), -1);
        double gas = bundle.getDouble(SensorsService.DataKeySensors.GAS.getKey(), -1);

        Measures measures = new Measures();
        measures.setOpen(door);
        measures.setTemp_1(temp1);
        measures.setTemp_2(temp2);
        measures.setTemp_3(temp3);
        measures.setHygro(humidity);
        measures.setGas(gas);

        return measures;
    }

    private void verifyTemp(Fridge fridge, Measures measures){
        if(isNotifTemp1Up) {
            if (fridge.isTemp_alert_max_on() && measures.getTemp_1() > fridge.getTemp_alert_max()) {
                sendNotification("temp_1", "{\"value\": " + measures.getTemp_1()  + ",\"type\":\"high\"}");
                isNotifTemp1Up = false;

                Log.i(TAG, "verifyTemp 1 high " + measures.getTemp_1());
            }
            if (fridge.isTemp_alert_min_on() && measures.getTemp_1() < fridge.getTemp_alert_min()) {
                sendNotification("temp_1", "{\"value\": " + measures.getTemp_1()  + ",\"type\":\"low\"}");
                isNotifTemp1Up = false;

                Log.i(TAG, "verifyTemp 1 low " + measures.getTemp_1());
            }
        } else if (measures.getTemp_1() < fridge.getTemp_alert_max() && measures.getTemp_1() > fridge.getTemp_alert_min()) {
            isNotifTemp1Up = true;
        }

        if(isNotifTemp2Up) {
            if (fridge.isTemp_alert_max_on() && measures.getTemp_2() > fridge.getTemp_alert_max()) {
                sendNotification("temp_2", "{\"value\": " + measures.getTemp_2()  + ",\"type\":\"high\"}");
                isNotifTemp2Up = false;

                Log.i(TAG, "verifyTemp 2 high " + measures.getTemp_2());
            }
            if (fridge.isTemp_alert_min_on() && measures.getTemp_2() < fridge.getTemp_alert_min()) {
                sendNotification("temp_2", "{\"value\": " + measures.getTemp_2()  + ",\"type\":\"low\"}");
                isNotifTemp2Up = false;

                Log.i(TAG, "verifyTemp 2 low " + measures.getTemp_2());
            }
        } else if (measures.getTemp_2() < fridge.getTemp_alert_max() && measures.getTemp_2() > fridge.getTemp_alert_min()) {
            isNotifTemp2Up = true;
        }

        if(isNotifTemp3Up) {
            if (fridge.isTemp_alert_max_on() && measures.getTemp_3() > fridge.getTemp_alert_max()) {
                sendNotification("temp_3", "{\"value\": " + measures.getTemp_3()  + ",\"type\":\"high\"}");
                isNotifTemp3Up = false;

                Log.i(TAG, "verifyTemp 3 high " + measures.getTemp_3());
            }
            if (fridge.isTemp_alert_min_on() && measures.getTemp_3() < fridge.getTemp_alert_min()) {
                sendNotification("temp_3", "{\"value\": " + measures.getTemp_3()  + ",\"type\":\"low\"}");
                isNotifTemp3Up = false;

                Log.i(TAG, "verifyTemp 3 low " + measures.getTemp_3());
            }
        } else if (measures.getTemp_2() < fridge.getTemp_alert_max() && measures.getTemp_3() > fridge.getTemp_alert_min()) {
            isNotifTemp3Up = true;
        }
    }

    private void verifyGas(Fridge fridge, Measures measures){
        if(!fridge.isGas_alert_on()){
            return;
        }
        if(isNotifGasUp) {
            if( fridge.getGas_alert_max() > measures.getGas() ){
                sendNotification("gas", Double.toString(measures.getGas()));
                isNotifGasUp = false;

                Log.i(TAG, "verifyGas > max " + measures.getGas());
            }
        } else  if( fridge.getGas_alert_max() < measures.getGas() ) {
            isNotifGasUp = true;
        }
    }

    private void verifyHumidity(Fridge fridge, Measures measures){
        if(!fridge.isHygro_alert_on()){
            return;
        }

        if(isNotifHygroUp) {
            if( fridge.getHygro_alert_max() > measures.getHygro() ) {
                sendNotification("hygro", Double.toString(measures.getHygro()));
                isNotifHygroUp = false;

                Log.i(TAG, "verifyHumidity > max " + measures.getHygro());
            }
        } else if( fridge.getHygro_alert_max() < measures.getHygro()) {
            isNotifHygroUp = true;
        }
    }

    private void verifyDoor(Fridge fridge, Measures measures){
        if(!fridge.isOpen_alert_on()){
            return;
        }
        if (lastDoorIsOpen && measures.isOpen()) {
            Log.i(TAG, "change door position: " + measures.isOpen());
            long diffInMillies = new Date().getTime() - openDate.getTime();
            if(diffInMillies > fridge.getOpen_alert_time()*1000) {
                Log.i(TAG, "Send timeout notification door !" + measures.isOpen());
                sendNotification("door_timeout", Integer.toString(fridge.getOpen_alert_time()));
                openDate = new Date();
            }
        }
        if(lastDoorIsOpen != measures.isOpen()){
            if(!lastDoorIsOpen && measures.isOpen()) {
                openDate = new Date();
            } else if(lastDoorIsOpen && !measures.isOpen()) {
                openDate = null;
            }
            sendNotification("door", measures.isOpen() ? "opened" : "closed");
            lastDoorIsOpen = measures.isOpen();
        }
    }

    private void sendNotification(String name, String val) {
        Fridge fridge = ControleurFridge.getInstance().getFridge();
        if(fridge == null) {
            Log.i(TAG, "Fridge is null...");
            return;
        }
        Event ev = new Event();
        ev.setName(name);
        ev.setValue(val);
        appelAPI.sendEvent(fridge.getSerial(), ev);
    }
}
