package com.rookie_fit.fridgesecond.Service;

import android.util.Log;

import com.rookie_fit.fridgesecond.CmdExecutor;

import java.io.DataOutputStream;
import java.io.IOException;


/**
 * Created by 53js-Seb on 13/03/2017.
 */

public class TempReader {
    private static native double readTemperature(String serialNumber);


    public void start(){
    }


    public double readTemp(String number){
        return readTemperature(number);
    }


    public void stop(){
        CmdExecutor.execute("echo 239 > /sys/class/gpio/unexport");
    }
}
