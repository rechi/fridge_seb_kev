package com.rookie_fit.fridgesecond.Service;

import android.os.Bundle;
import android.os.Handler;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Constantin on 26/03/2017.
 */

public class SensorsManager {

    static {
        System.loadLibrary("jni");
    }

    private final int PORT_PORTE = 239;
    /*private final String TEMPERATURE_1 = "28-80000027170d";
    private final String TEMPERATURE_2 = "28-80000026af03";
    private final String TEMPERATURE_3 = "28-80000026b631";*/
    private final int PORT_HUMIDITE = 237;
    private final int PORT_GAZ_DIGITAL = 236;
    private final int PORT_GAZ_ANALOG = 0;// ADC.AIN0
    private boolean mStop;
    private Process p;

    public SensorsManager() {
        initialize();
    }

    private void initialize() {
        try {
            p = Runtime.getRuntime().exec(new String[]{"su", "-c", "system/bin/sh"});
            //from here all commands are executed with su permissions
            System.out.println("Permission recuperée");
            DataOutputStream stdin = new DataOutputStream(p.getOutputStream());
            stdin.writeBytes("mount -o rw,remount /system\n"); // \n executes the command
            stdin.writeBytes("depmod -a\n"); // \n executes the command
            stdin.writeBytes("modprobe w1-gpio\n"); // \n executes the command
            stdin.writeBytes("modprobe w1-therm\n"); // \n executes the command
            System.out.println("mount, depmod, modprobe executed");
            stdin.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean digitalReadByPin(int port) {
        return (digitalRead(port) == 0) ? false : true;
    }

    public double getTemperatureBySerial(String serialNumber) {
        return readTemperature(serialNumber);
    }

    public float getHumidity(int port) {
        float[] humidity = readHumidity(port);
        //if the data is not correctly readed we try again after 3 sec sleep
        if(humidity[0] == 0.00f && humidity[1] == 999.00f) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            humidity = readHumidity(PORT_HUMIDITE);
        }
        return humidity[0];//For now we return only the humidity
    }

    public float getGasPPM(int port) {
        return readPPM(port);
    }

    /*
    public void update() {
        System.out.println("WIRINGPI Getting value of DOOR State (GPIO239) = " + digitalRead(PORT_PORTE));

        System.out.println("Getting value of " + TEMPERATURE_1  + " (TMP1) = " + (double)readTemperature(TEMPERATURE_1));
        System.out.println("Getting value of " + TEMPERATURE_2  + " (TMP2) = " + (double)readTemperature(TEMPERATURE_2));
        System.out.println("Getting value of " + TEMPERATURE_3 + " (TMP3) = " + (double)readTemperature(TEMPERATURE_3));

        float[] humidity = readHumidity(PORT_HUMIDITE);
        //if the data is not correctly readed we try again after 3 sec sleep
        if(humidity[0] == 0.00f && humidity[1] == 999.00f) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            humidity = readHumidity(PORT_HUMIDITE);
        }
        System.out.println("Getting value of " + PORT_HUMIDITE + " (DHT22)  = " + (float)humidity[0] + "% ," + (float)humidity[1] + "°C");
        System.out.println("Getting value of " + PORT_GAZ_ANALOG + " (MQ135) = " + (float)readPPM(PORT_GAZ_ANALOG));

        System.out.println("-----------------------------------------------");
    }*/

    public int setupWiringPi() {
        System.out.println("WIRING PI SETTING");
        return wiringPiSetupGpio();
    }


    public boolean exportGPIO() {
        try {
            System.out.println("Exporting...");
            DataOutputStream stdin = new DataOutputStream(p.getOutputStream());
            //EXPORT PORT_PORTE
            stdin.writeBytes("echo " + PORT_PORTE + " > /sys/class/gpio/export\n"); // \n executes the command
            System.out.println("Export of " + PORT_PORTE + " launched");
            stdin.writeBytes("echo out > /sys/class/gpio/gpio" + PORT_PORTE + "/direction\n");
            stdin.writeBytes("chmod 777 /sys/class/gpio/gpio" + PORT_PORTE + "/value\n");
            System.out.println("Direction of " + PORT_PORTE + " setted");
            //EXPORT PORT_HUMIDITE
            stdin.writeBytes("echo " + PORT_HUMIDITE + " > /sys/class/gpio/export\n"); // \n executes the command
            System.out.println("Export of " + PORT_HUMIDITE + " launched");
            stdin.writeBytes("echo in > /sys/class/gpio/gpio" + PORT_HUMIDITE + "/direction\n");
            stdin.writeBytes("chmod 777 /sys/class/gpio/gpio" + PORT_HUMIDITE + "/direction\n");
            stdin.writeBytes("chmod 777 /sys/class/gpio/gpio" + PORT_HUMIDITE + "/value\n");
            System.out.println("Direction of " + PORT_HUMIDITE + " setted");
            stdin.writeBytes("chmod 777 /dev/gpiomem\n");
            stdin.writeBytes("chmod 777 /dev/gpioctrl\n");//Used for accessing Humidity and Gas
            stdin.writeBytes("chmod 777 /dev/mem\n");
            stdin.writeBytes("exit\n");
            stdin.flush();
            DataInputStream is = new DataInputStream(p.getErrorStream());
            BufferedReader d = new BufferedReader(new InputStreamReader(is));
            String inErr;
            System.out.println("Flux d'erreur:");
            while ((inErr = d.readLine()) != null) {
                System.out.println(inErr);
            }
            System.out.println("Flux d'erreur: FIN");
            Thread.sleep(1000);
        } catch (Exception e1) {
            e1.printStackTrace();
            return false;
        }
        return true;
    }

    boolean unexportGPIO() {
        try {
            DataOutputStream os = new DataOutputStream(p.getOutputStream());
            os.writeBytes("echo " + PORT_PORTE + " > /sys/class/gpio/unexport\n");
            os.flush();
            /*DataInputStream is = new DataInputStream(p.getErrorStream());
            BufferedReader d = new BufferedReader(new InputStreamReader(is));
            String inErr;
            System.out.println("Flux d'erreur:");
            while ((inErr = d.readLine()) != null) {
                System.out.println(inErr);
            }
            System.out.println("Flux d'erreur: FIN");*/
        } catch (IOException e1) {
            e1.printStackTrace();
            return false;
        }
        return true;
    }

    public native int wiringPiSetupSys();
    public native int wiringPiSetupGpio();
    public native int analogRead(int port);
    public native void digitalWrite(int port, int onoff);
    public native int digitalRead(int port);
    public static native double readTemperature(String serialNumber);
    public static native int readDoorState( int pin);
    public static native float[] readHumidity(int pin);
    public static native float readPPM(int pin);
}
