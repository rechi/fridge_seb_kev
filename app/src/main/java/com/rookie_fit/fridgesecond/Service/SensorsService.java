package com.rookie_fit.fridgesecond.Service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.rookie_fit.fridgesecond.API.AppelAPI;
import com.rookie_fit.fridgesecond.ControleurFridge;
import com.rookie_fit.fridgesecond.Entity.Fridge;
import com.rookie_fit.fridgesecond.Entity.Measures;
import com.rookie_fit.fridgesecond.R;


import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by 53js-Seb on 07/03/2017.
 */

public class SensorsService extends Service {

    private static final String TAG = "SensorsService";

    // ---- CONSTANTES
    private String [] temperatures;

    // ---- READER SENSORS
    /*private TempReader tempReader = new TempReader();
    private DoorReader doorReader = new DoorReader();
    private HumidityReader humidityReader = new HumidityReader();
    private GasReader gasReader = new GasReader();*/


    private int portPorte;
    private int portHumidite;
    private int portGazDigital;
    private int portGazAnalog;// should be ADC.AIN0 default

    // ---- COMMUNICATION WITH BIND
    private Bundle sensorsData;
    public synchronized Bundle getDataSensors(){
        return sensorsData;
    }

    private synchronized void setDataSensors(Bundle bundle){
        sensorsData = bundle;
    }

    private SensorsBinder sensorsBinder = new SensorsBinder();
    private class SensorsBinder extends Binder {
        SensorsService getService(){
            return SensorsService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) { return sensorsBinder; }


    // ---- MANAGE TIMER
    private Timer timerReadData = new Timer();
    private Timer timerSendData = new Timer();

    private Timer timerUpdateFridge = new Timer();
    private NotificationService notificationService;

    private SensorsManager serviceManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "Init timers ! ", Toast.LENGTH_SHORT).show();

        //init const
        temperatures = getApplicationContext()
                .getResources().getStringArray(R.array.sensors_ref_temperatures);

        portPorte = getResources().getInteger(R.integer.sensors_gpio_door);
        portHumidite = getResources().getInteger(R.integer.sensors_gpio_humidity);
        portGazAnalog = getResources().getInteger(R.integer.sensors_gpio_gas_analog);

        //Start sensor readers

        serviceManager = new SensorsManager();
        serviceManager.exportGPIO();
        serviceManager.setupWiringPi();

        /*tempReader.start();
        humidityReader.start();
        gasReader.start();
        doorReader.start(getApplicationContext().
                getResources().getInteger(R.integer.sensors_gpio_door));*/

        //Start timerReadData
        timerReadData.schedule(new ReadSensorsTimer(), getResources().getInteger(R.integer.sensors_service_read_data_delay),
                getResources().getInteger(R.integer.sensors_service_read_data_period));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //Start timer to send data to API
        timerSendData.schedule(new SendDataSensorTimer(), getResources().getInteger(R.integer.sensors_service_send_data_delay),
                getResources().getInteger(R.integer.sensors_service_send_data_period));

        //Start notification service
        notificationService = new NotificationService(this);
        timerUpdateFridge.schedule(notificationService,  getResources().getInteger(R.integer.sensors_service_update_fridge_delay),
                getResources().getInteger(R.integer.sensors_service_update_fridge_period));

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Destroy timers...", Toast.LENGTH_SHORT).show();

        //Close timerReadData
        timerReadData.cancel();
        timerReadData.purge();

        //Close timerSendData
        timerSendData.cancel();
        timerSendData.purge();


        //Close timerSendData
        timerUpdateFridge.cancel();
        timerUpdateFridge.purge();

        serviceManager.unexportGPIO();

        //Close sensor readers
        //Not needed refoactor to sensorsManager
       /* tempReader.stop();
        humidityReader.start();
        gasReader.start();
        doorReader.stop();*/

        super.onDestroy();
    }

    // ---- READ SENSOR TIMER
    private class ReadSensorsTimer extends TimerTask {
        private Intent intent = new Intent(getString(R.string.sensors_service_action));

        @Override
        public void run() {
            Bundle data = new Bundle();

            //Read sensors
            System.out.println("---------------READ DATA SENSORS--------------------------------");

            boolean door = serviceManager.digitalReadByPin(portPorte);
            System.out.println("DOOR STATE = " + door);
            data.putBoolean(DataKeySensors.DOOR.key, door);

            double tp1 = serviceManager.getTemperatureBySerial(temperatures[0]);
            System.out.println("TEMP1 = " + tp1);
            data.putDouble(DataKeySensors.TEMP_0.key, tp1);

            double tp2 = serviceManager.getTemperatureBySerial(temperatures[1]);
            System.out.println("TEMP2 = " + tp2);
            data.putDouble(DataKeySensors.TEMP_1.key, tp2);

            double tp3 = serviceManager.getTemperatureBySerial(temperatures[2]);
            System.out.println("TEMP3 = " + tp3);
            data.putDouble(DataKeySensors.TEMP_2.key, tp3);

            float humidity = serviceManager.getHumidity(portHumidite);
            System.out.println("HUMIDITY = " + humidity);
            data.putDouble(DataKeySensors.HUMIDITY.key, humidity);

            float gas = serviceManager.getGasPPM(portGazAnalog);
            System.out.println("GAS = " + gas);
            data.putDouble(DataKeySensors.GAS.key, gas);
            System.out.println("--------------------------------------------------------------");
            /*
            data.putBoolean(DataKeySensors.DOOR.key, Math.random() < 0.5);
            data.putDouble(DataKeySensors.TEMP_0.key, Math.random());
            data.putDouble(DataKeySensors.TEMP_1.key, Math.random());
            data.putDouble(DataKeySensors.TEMP_2.key, Math.random());
            data.putDouble(DataKeySensors.HUMIDITY.key, Math.random());
            data.putDouble(DataKeySensors.GAS.key, Math.random());*/

            //Notify
            intent.putExtras(data);
            sendBroadcast(intent);
            setDataSensors(data);

            if(notificationService != null){
                notificationService.updateDataSensor(data);
            }
        }
    }

    // ---- SEND DATA SENSOR TIMER
    private class SendDataSensorTimer extends TimerTask {

        private AppelAPI appelAPI = new AppelAPI();

        @Override
        public void run() {
            Bundle bundle = getDataSensors();
            if(bundle == null){
                Log.i(TAG, "SendDataSensorTimer bundle null...");
                return;
            }

            boolean door = bundle.getBoolean(SensorsService.DataKeySensors.DOOR.getKey(), false);
            double temp1 = bundle.getDouble(SensorsService.DataKeySensors.TEMP_0.getKey(), -1);
            double temp2 = bundle.getDouble(SensorsService.DataKeySensors.TEMP_1.getKey(), -1.);
            double temp3 = bundle.getDouble(SensorsService.DataKeySensors.TEMP_2.getKey(), -1.);
            double humidity = bundle.getDouble(SensorsService.DataKeySensors.HUMIDITY.getKey(), -1);
            double gas = bundle.getDouble(SensorsService.DataKeySensors.GAS.getKey(), -1);

            Measures measures = new Measures();
            measures.setOpen(door);
            measures.setTemp_1(temp1);
            measures.setTemp_2(temp2);
            measures.setTemp_3(temp3);
            measures.setHygro(humidity);
            measures.setGas(gas);


            Fridge fridge = ControleurFridge.getInstance().getFridge();
            if(fridge != null){
                appelAPI.sendDataSensors(fridge.getSerial(), measures);
            }

        }
    }

    // ---- ENUM DataKeySensors
    public enum DataKeySensors {
        TEMP_0("TEMP_0"),
        TEMP_1("TEMP_1"),
        TEMP_2("TEMP_2"),
        DOOR("DOOR"),
        HUMIDITY("HUMIDITY"),
        GAS("GAS");

        private String key;

        DataKeySensors(String key){
            this.key = key;
        }

        public String getKey(){
            return key;
        }
    }
}
