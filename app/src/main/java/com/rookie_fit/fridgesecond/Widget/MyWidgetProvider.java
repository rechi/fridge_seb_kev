package com.rookie_fit.fridgesecond.Widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.widget.RemoteViews;

import com.rookie_fit.fridgesecond.ControleurFridge;
import com.rookie_fit.fridgesecond.R;

/**
 * Created by rechi on 06/02/2017.
 */

public class MyWidgetProvider extends AppWidgetProvider {

    private static final String ACTION_CLICK = "ACTION_CLICK";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {

        System.out.println("OK");


        // Retrouver tous les id
        ComponentName thisWidget = new ComponentName(context,
                MyWidgetProvider.class);
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        for (int widgetId : allWidgetIds) {

            RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.widget_layout);
            remoteViews.setTextViewText(R.id.temperatureText, "?");
            remoteViews.setTextViewText(R.id.temperatureText1, "?");
            remoteViews.setTextViewText(R.id.temperatureText2, "?");
            remoteViews.setTextViewText(R.id.gasText, "?");
            remoteViews.setTextViewText(R.id.doorText, "?");
            remoteViews.setTextViewText(R.id.hydroText, "?");

            // Définir le texte
            //remoteViews.setTextViewText(R.id.temperatureText, " 15"+ (char) 0x00B0 +"C ");


            ControleurFridge.getInstance(null,getDeviceId(context));

           // new AppelAPI().getFridge("423568",remoteViews,appWidgetManager,widgetId);



            // Enregistrer un onClickListener
            Intent intent = new Intent(context, MyWidgetProvider.class);

            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                    0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.refresh, pendingIntent);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
    }

    public static String getDeviceId(Context context) {
        final String deviceId = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        if (deviceId != null) {
            return deviceId;
        } else {
            return android.os.Build.SERIAL;
        }
    }
}
