/*
  Programme de test du MQ135 permettant de mesurer la présence de polluants dans l'atmosphère (CO, CO2 Alcool, fumées...)
  Pour plus d'infos ici http://www.projetsdiy.fr/mq135-mesure-qualite-air-polluant-arduino
  Utilise la librairie mq135.cpp mise à disposition par Georg Krocker https://github.com/GeorgK/MQ135
  Projets DIY - Mars 2016 - www.projetsdiy.fr
*/
#ifdef __cplusplus
extern "C" {
#endif
#include "MQ135.hpp"
const int mq135Pin = 0;     // Pin sur lequel est branché de MQ135



float readRZERO(int pin)
{
    MQ135 gasSensor = MQ135(pin);  // Initialise l'objet MQ135 sur le Pin spécifié
    return gasSensor.getRZero();
}

float readPPM(int pin)
{
    MQ135 gasSensor = MQ135(pin);  // Initialise l'objet MQ135 sur le Pin spécifié
    return gasSensor.getPPM();
}
#ifdef __cplusplus
}
#endif