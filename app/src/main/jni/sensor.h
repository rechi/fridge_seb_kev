//
// Created by Constantin on 20/02/2017.
//

#ifndef APP_C_JNI_TEMPERATURE_H
#define APP_C_JNI_TEMPERATURE_H

#endif //APP_C_JNI_TEMPERATURE_H


extern float readTemperature(char *serialNumber);

extern int readDoorState(int pin);