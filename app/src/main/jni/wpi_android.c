#include <jni.h>
#include <android/log.h>
#include <stdio.h>
#include <string.h>

#include <android/log.h>
#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOG_TAG "wpi_android"
#define printf	LOGI
typedef unsigned long __mmap_t;

#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"

#include "unistd.h"
#include "string.h"
#include "time.h"

#include "wiringPi.h"
#include "wiringPiI2C.h"
#include "wiringSerial.h"
#include "lcd.h"
#include "sensor.h"
#include "dht22.h"
#include "MQ135reader.h"

jint Java_com_rookie_1fit_fridgesecond_Service_SensorsManager_analogRead(JNIEnv* env, jobject obj, jint port) {
    return analogRead(port);
}

jint Java_com_rookie_1fit_fridgesecond_Service_SensorsManager_digitalRead(JNIEnv* env, jobject obj, jint port) {
    return digitalRead(port);
}

void Java_com_rookie_1fit_fridgesecond_Service_SensorsManager_digitalWrite(JNIEnv* env, jobject obj, jint port, jint onoff) {
    digitalWrite(port, onoff);
}

int Java_com_rookie_1fit_fridgesecond_Service_SensorsManager_wiringPiSetupSys(JNIEnv* env, jobject obj) {
    putenv("WIRINGPI_DEBUG=1");
    wiringPiSetupSys();
    return 0;
}

int Java_com_rookie_1fit_fridgesecond_Service_SensorsManager_wiringPiSetupGpio(JNIEnv* env, jobject obj) {
    //putenv("WIRINGPI_DEBUG=1");
    wiringPiSetupGpio();
    return 0;
}


/*JNIEXPORT jdouble JNICALL Java_mbds_app_1c_1jni_MainActivity_readTemperature(JNIEnv * env, jobject jobj, jstring serialNumber) {
    char *serialNumberChar;
    serialNumberChar = (*env)->GetStringUTFChars(env, serialNumber, NULL);
    printf(serialNumberChar);
    return readTemperature(serialNumberChar);
}*/

JNIEXPORT jdouble JNICALL Java_com_rookie_1fit_fridgesecond_Service_SensorsManager_readTemperature(JNIEnv *env, jobject obj, jstring javaString)
{
    const char *nativeString = (*env)->GetStringUTFChars(env, javaString, 0);

    float value = readTemperature(nativeString);

    (*env)->ReleaseStringUTFChars(env, javaString, nativeString);

    return value;
}

JNIEXPORT jfloatArray JNICALL Java_com_rookie_1fit_fridgesecond_Service_SensorsManager_readHumidity(JNIEnv* env, jobject obj, jint port) {
    jfloatArray result;
    result = (*env)->NewFloatArray(env, 2);
    float* data;
    data = malloc(sizeof(float) * 2);
    data[0] = 123;  data[1] = 456;
    data = readHumidity(port);
    printf(" %.2f , %.2f", data[0], data[1]);
    (*env)->SetFloatArrayRegion(env, result, 0, 2, data);
    //free(data);
    return result;
}

JNIEXPORT jfloat JNICALL Java_com_rookie_1fit_fridgesecond_Service_SensorsManager_readPPM(JNIEnv *env, jobject obj,
                                                                    jint port) {
    return readPPM(port);
}

JNIEXPORT jfloat JNICALL Java_com_rookie_1fit_fridgesecond_Service_SensorsManager_readRZERO(JNIEnv *env, jobject obj, jint port)
{
    return readRZERO(port);
}


jint Java_com_rookie_1fit_fridgesecond_Service_SensorsManager_readDoorState(JNIEnv* env, jobject obj, jint port) {
    return readDoorState(port);
}